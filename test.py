# day 1
# for i in range(1,6,2):
# print(i)
# squares=[1,4,9,16,25]
# squares.append(7)
# squares.reverse()#倒置
# print(squares)
# a, b=0, 1
# while a<11:
#   print(b,end=',')
#    a,b=b,a+b


# day 2
# 水仙花数
# for i in range(10,6):
#     if (i%10)**3+((i%100)//10)**3+(i//100)**3==i:
#     print(i)

# 循环中的else语句
# i=0
# for n in range(2, 10):
#       for x in range(2, n):
#          if n % x == 0:
#               print(' ',n, 'equals', x, '*', n//x)
#               i=x
#               break
#       else:
#         print(i,'#',n, 'is a prime number')

# return用法
# def fib2(n):
#     result = []
#     a, b = 0, 1
#     while a < n:
#         result.append(a)
#         a, b = b, a+b
#     return result
# f100=fib2(100)
# print(f100)

# 列表
# list_a=[1,2,3,4,5,6,7,8]
# print(list_a)
# list_a.append(9)
# print('加一个元素',list_a)
# list_a.extend('abc')
# print(list_a)
# i=list_a.pop(9)
# print('删除',i,list_a)
# list_a.insert(9,'a')
# print(list_a)
# list_a.remove('a')
# print(list_a)

# 排序
# list_b=[11,74,48,65,32,1,24,1,31]
# for i in range(len(list_b)):
#     for j in range(len(list_b)):
#         if list_b[i]<=list_b[j]:
#             a=list_b[i]
#             list_b[i] = list_b[j]
#             list_b[j] = a
# list_b.sort()
# print(list_b)

# 元组
# list_yuan=(1,2,'a')
# print(list_yuan)

# 集合
# list_jihe={'dasdasda'}
# print(list_jihe)
# i='z' in list_jihe
# print(i)
# list_jihe2=set('zxcvdfxv')
# print(list_jihe2)

# 字典
# list_dict={'2150810321':'晓晨夜','1230180512':'夜橙筱'}
# for i,j in list_dict.items():
#    print(i,j)
# print(list_dict)


# 模块
# import function,sys
# from function import run
# if __name__=="__function__":#调用其他模块
#    pass
# 模块搜索路径
# import sys
# print(sys.path)
# print(dir(function))
# dir(sys)





# import socket
# so_cli=socket.socket()
# so_cli.connect(('10.31.4.27',8080))
# while True:
#      msg = input("请输入要发送的消息")
#      so_cli.send(msg.encode('UTF-8'))
#      data = so_cli.recv(1024).decode('UTF-8')
#      print('接受到的信息是',data)





# day 3
#f = open('text','r+')
#import json
#if __name__ == '__main__':
#  with open('a.json', 'w') as f:
#     info = {'1230180512': '夜橙筱'}
#     json.dump(info, f)
#     print(info)
#with open('a.json','r')as f:
#    info = json.loads(f.read())
#    for k,v in info.items():
#     print(k, v)
# class Glory:
#    id='yeqiu'
#    def register(self):
#       print('welcome to gelin forest')
# x=Glory()s
# x.register()
#DerivedClassname 派生类
#__spam 私有变量
#tcp建立、链接、销毁#
#import os

#if __name__ =='__main__':
#     os.system('start http:\\ff14.huijiwiki.com')
# from urllib.request import urlopen
# with urlopen('http://www.baidu.com') as response:
#      for line in response:
#          line = line.decode('utf-8')  # Decoding the binary data to text.
#          print(line)

# from datetime import date
# now = date.today()
# print(now)
# now.strftime("%m-%d-%y. %d %b %Y is a %A on the %d day of %B.")
# birthday = date(1964, 7, 31)
# age = now - birthday
# print(age.days)

#1970.01.01 00:00
# try:
#     a=int(input('输入'))
# except :
#    print('请不要做多余的事')


#day 4

#git status
#git commit -m "my_project.py changed"

#$ cd /d/pythonwork
#$ git remote add origin https://gitea.com/sleep_summer/work.git
#$ git push -u origin master

